const router = require('express').Router();

router.route('/').get((req, res) => {
    msg = `Hello ${req.query.name || "World"} from get request :=), nice!`;
    res.json({msg});
});

router.route('/:lang').get((req,res) => {
    switch(req.params.lang){
        case "es":
            msg = "Hola mundo en espaniol";
            break;
        case "mex":
            msg = "Que ondas mundo en espaniol";
            break;
        case "en":
            msg = "Hello world in english";
            break;
        default:
            msg = "Que pedo/ondas man";
    }
    res.json({msg});
})

router.route('/').post((req,res) => {
    msg = `Hello ${req.body.name || "World"} from post request :)`;
    res.json({msg});
})

module.exports = router;